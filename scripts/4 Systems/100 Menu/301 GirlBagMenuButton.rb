module UI
  # Button that is shown in the main menu for girl bag
  class GirlBagMenuButton < PSDKMenuButtonBase
    private

    # Get the icon index
    # @return [Integer]
    def icon_index
      7
    end
  end
end
